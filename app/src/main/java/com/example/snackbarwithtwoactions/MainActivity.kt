package com.example.snackbarwithtwoactions

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.SnackbarContentLayout
import android.support.v7.widget.AppCompatButton
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.Button
import android.widget.LinearLayout

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val snack = Snackbar.make(findViewById(R.id.the_view), "", 60000)

        snack.setText("Some text")
        snack.setAction("First button") {

        }

        val snackLayout = snack.view as Snackbar.SnackbarLayout
        val snackContent = snackLayout.getChildAt(0) as SnackbarContentLayout

        val b2 = LayoutInflater.from(this).inflate(R.layout.the_button, null)
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        ).apply {
            weight = 1.0f
            gravity = Gravity.CENTER_VERTICAL or Gravity.RIGHT or Gravity.END
        }
        b2.layoutParams = params

        snackContent.addView(b2)
        snack.show()
    }
}